var chainModel = avalon.define({
    $id: 'chainVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    joinText: [
        {title: '品牌支持', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '配送支持', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '营销策划', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '人员培训', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '产品更新', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '信息推送', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'}
    ],

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    changeActiveDishesIndex: function (index) {
        chainModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    chainModel.init();
}, 100);
