var productDetailModel = avalon.define({
    $id: 'productDetailVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    changeActiveDishesIndex: function (index) {
        productDetailModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    productDetailModel.init();
}, 100);
