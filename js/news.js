var newsModel = avalon.define({
    $id: 'newsVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    goDetail: function () {
        var url = window.location.href;
        url = url.substring(0, url.lastIndexOf('/'));
        window.location.href = url + '/newsDetail.html';
    },
    changeActiveDishesIndex: function (index) {
        newsModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    newsModel.init();
}, 100);
