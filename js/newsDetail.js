var newsDetailModel = avalon.define({
    $id: 'newsDetailVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    changeActiveDishesIndex: function (index) {
        newsDetailModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    newsDetailModel.init();
}, 100);
