var aboutModel = avalon.define({
    $id: 'aboutVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    changeActiveDishesIndex: function (index) {
        aboutModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    aboutModel.init();
}, 100);
