var homeModel = avalon.define({
    $id: 'homeVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    joinText: [
        {title: '品牌支持', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '配送支持', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '营销策划', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '人员培训', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '产品更新', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'},
        {title: '信息推送', text: '公司秉承“诚信为先、服务是本、绿色健康、顾客至上”的经营理念，坚持“创业发展、宁静致远、无私众容、双赢是金”的经营方针。'}
    ],

    goMenu: function (pageName) {
        var url = window.location.href;
        url = url.substring(0, url.lastIndexOf('/'));
        window.location.href = url + '/' + pageName + '.html';
    },

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
        // 顶部
        new Swiper ('#index-top-swiper-container', {
            loop: true,
            autoplay: true,
            grabCursor : true,
            roundLengths : true,
            autoHeight: true,

            // 如果需要分页器
            pagination: {
                el: '#index-top-swiper-pagination',
            }
        })
    },
    changeActiveDishesIndex: function (index) {
        homeModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    homeModel.init();
}, 100);
