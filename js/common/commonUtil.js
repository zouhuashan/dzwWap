
var rUtil = {
    projectName: 'dzwwx',

    // 获取url上面的参数
    parseUrl : function(key){
        var param = {};
        var url = document.URL;
        try{
            var _index1 = url.indexOf("?");
            if(_index1 > -1){
                var str = url.substring(_index1 + 1);
                var _index2 = str.indexOf("&");
                if(_index2 > -1){
                    //多个参数
                    var arr = str.split("&");
                    for(var i = 0, len = arr.length; i < len; i++){
                        var _index3 = arr[i].indexOf("=");
                        if(_index3 > -1){
                            var arr1_temp = arr[i].split("=");
                            param[arr1_temp[0]] = arr1_temp[1];
                        }
                    }
                }else{
                    //单个参数
                    var _index4 = str.indexOf("=");
                    if(_index4 > -1){
                        var arr2_temp = str.split("=");
                        param[arr2_temp[0]] = arr2_temp[1];
                    }
                }
            }
        }catch(e){
            throw "util.parseUrl解析异常";
        }
        if(!!key) return param[key];
        return param;
    }
}

//加载依赖包
// jquery
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/dist/jquery.min.js' ></script>");
// avalon
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/dist/avalon.js' ></script>")

// 通用HTML代码
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/common/commonHtml.js' ></script>");
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/common/commonAvalon.js' ></script>");

// 加载通用的css
document.write("<link rel='stylesheet' charset='UTF-8' href='/" + rUtil.projectName + "/css/common.css'></link>");
document.write("<link rel='stylesheet' charset='UTF-8' href='/" + rUtil.projectName + "/css/menu_style.css'></link>");

// designWidth:设计稿的实际宽度值，需要根据实际设置
// maxWidth:制作稿的最大宽度值，需要根据实际设置
// 这段js的最后面有两个参数记得要设置，一个为设计稿实际宽度，一个为制作稿最大宽度，例如设计稿为750，最大宽度为750，则为(750,750)
;(function (designWidth, maxWidth) {
    var doc = document,
        win = window,
        docEl = doc.documentElement,
        remStyle = document.createElement("style"),
        tid;

    function refreshRem () {
        var width = docEl.getBoundingClientRect().width;
        maxWidth = maxWidth || 320;
        width > maxWidth && (width = maxWidth);
        var rem = width / designWidth * 200;
        remStyle.innerHTML = 'html{font-size:' + rem + 'px;}';
    }

    if (docEl.firstElementChild) {
        docEl.firstElementChild.appendChild(remStyle);
    } else {
        var wrap = doc.createElement("div");
        wrap.appendChild(remStyle);
        doc.write(wrap.innerHTML);
        wrap = null;
    }
    // 要等 wiewport 设置好后才能执行 refreshRem，不然 refreshRem 会执行2次；
    refreshRem();

    win.addEventListener("resize", function () {
        clearTimeout(tid); // 防止执行两次
        tid = setTimeout(refreshRem, 50);
    }, false);

    win.addEventListener("pageshow", function (e) {
        if (e.persisted) { // 浏览器后退的时候重新计算
            clearTimeout(tid);
            tid = setTimeout(refreshRem, 50);
        }
    }, false);

    if (doc.readyState === "complete") {
        doc.body.style.fontSize = "16px";
    } else {
        doc.addEventListener("DOMContentLoaded", function (e) {
            doc.body.style.fontSize = "16px";
        }, false);
    }
})(750, 750);