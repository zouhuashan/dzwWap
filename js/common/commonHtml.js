var commHtml = {

    // 通用顶部菜单
    topMenueHtml: [
        '<div ms-controller="commonTopMenueVMId" class="common-menu-nav">' +
        '   <div class="common-menu-item" ms-click="@goMenu(\'index\')" ms-class="[@activeIndex == 0 ? \'active\' : \'\']">' +
        '       <img src="img/menu1.png"/>' +
        '       <span>首页</span>' +
        '   </div>' +
        '   <div class="common-menu-item" ms-click="@goMenu(\'chain\')" ms-class="[@activeIndex == 1 ? \'active\' : \'\']">' +
        '       <img src="img/menu2.png"/>' +
        '       <span>连锁加盟</span>' +
        '   </div>' +
        '<div class="common-menu-item" ms-click="@goMenu(\'server\')" ms-class="[@activeIndex == 2 ? \'active\' : \'\']">' +
        '       <img src="img/menu3.png"/>' +
        '       <span>客服中心</span>' +
        '   </div>' +
        '   <div class="common-menu-item" ms-click="@goMenu(\'about\')" ms-class="[@activeIndex == 3 ? \'active\' : \'\']">' +
        '       <img src="img/menu4.png"/>' +
        '       <span>关于我们</span>' +
        '   </div>' +
        '</div>'
    ]
}