//通用顶部的菜单av
var commonTopMenueModel = avalon.define({
    $id: 'commonTopMenueVMId',

    activeIndex: '5',

    goMenu: function (pageName, type) {
        var url = window.location.href;
        url = url.substring(0, url.lastIndexOf('/'));
        window.location.href = url + '/' + pageName + '.html' + (type != undefined ? ('?type=' + type) : '');
    },
    init: function () {
        setTimeout(function () {
            // 通过浏览器的URL来选中顶部的菜单
            var url = window.location.href;
            if (url.indexOf("index") >= 0) {
                commonTopMenueModel.activeIndex = 0;
            } else if (url.indexOf("chain") >= 0) {
                commonTopMenueModel.activeIndex = 1;
            } else if (url.indexOf("server") >= 0) {
                commonTopMenueModel.activeIndex = 2;
            } else if (url.indexOf("about") >= 0) {
                commonTopMenueModel.activeIndex = 3;
            }
        }, 100)
    }
});

commonTopMenueModel.init();
