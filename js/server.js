var serverModel = avalon.define({
    $id: 'serverVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    menuHtml: commHtml.topMenueHtml,
    init: function () {
    },
    changeActiveDishesIndex: function (index) {
        serverModel.activeDishesIndex = index
    }
});

setTimeout(function() {
    serverModel.init();
}, 100);
